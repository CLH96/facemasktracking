# FaceMaskTracking

Data and code to reproduce figures and statistics using Python 3 and jupyter notebooks 

Example (speech envelope) already visible in gitlab (just click on 'StatisticandPlots.ipynb')

need packages:

numpy
pandas
pymatreader
seaborn
pingouin
